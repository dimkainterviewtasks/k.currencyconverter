

Here I will try to answer on some possible questions which could be raised when you see this implementation:

1.	Why do we have Domain folder for such a simple application?
		Well I can see at least one bussines rule (MAX number is 999999999). And BR should be checked in domain.

2.	What is the CurrencyService for?
	Why Currency is internal?
	What is the CurrencyDto for?
	Why CurrencyDto doesn't contain currency sign?
	 Currency is internal because it's a domain object and we need to keep it protected.
	 CurrencyService is a thing which knows how to communicate with domain.
		It contains antycorruption layer don't allow wrong data to flow to domain.
		If data is correct it know how input data to connvert to domains data.
	 CurrencyDto needs to transfer information to client. You need something if you don't want to share domain structure.

3. Why do parsing numbers to words code is in Host application?
	Well, Console app in the same time is a host app and presentation app.
	User can communicate with the application by this project.
	And I belive the parsing belongs to presentation layer because if you want other output of data you don't need to change domain now.
	By the way now you just to need to implement new implementation of IExpressionVisitor and reuse it in WordBuilder.
	But I guess for test task is ok to go with such implementation.

4. Why do I use visitor pattern?
	Well the code were working with different implementation. You can take a look on it (commit "Moved presentation logic from domain." 02.10.2019)
	But this implementation were realy unreadable. But it contained much less files. Well there is no silver bullet. 

4. Why do the projects use netcore & netstandart?
	I use GitLab CI pipeline to check code when I commit something. By this reason I want to use net SDK to compile code & run tests.