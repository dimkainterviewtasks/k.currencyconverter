﻿using System;
using Domain.Application;
using NUnit.Framework;

namespace Domain.Tests.Domain
{
    [TestFixture]
    internal partial class CurrencyServiceTests
    {
        internal sealed class WhenInputValuesAreWrong
        {
            [TestCase("100d100")]
            [TestCase("100 . 100")]
            [TestCase("100.100")]
            public void ShouldRaiseException(string value)
            {
                // Act & Assert
                Assert.Throws<ArgumentException>(() =>
                    new CurrencyService().GetCurrency(value));
            }
        }

        internal sealed class WhenInputValuesAreCorrect
        {
            [TestCase("0", 0)]
            [TestCase("-1", -1)]
            [TestCase("1", 1)]
            [TestCase("25,5", 25.5)]
            [TestCase("00,01", 0.01)]
            [TestCase("45100", 45100)]
            [TestCase("45 100", 45100)]
            [TestCase("999999999", 999999999)]
            [TestCase("999 999 999", 999999999)]
            [TestCase("999 999 999,99", 999999999.99)]
            public void ShouldRaiseException(string value, decimal currencyValue)
            {
                // Arrange
                var service = new CurrencyService();

                // Act
                var currency = service.GetCurrency(value);
                
                // Assert
                Assert.That(currency.Value, Is.EqualTo(currencyValue));
            }
        }
    }
}