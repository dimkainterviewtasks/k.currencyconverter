﻿using System;
using NUnit.Framework;

namespace Domain.Tests.Domain
{
    [TestFixture]
    internal partial class CurrencyTest
    {
        internal sealed class WhenInputIsMoreThenMaxValue
        {
            [Test]
            public void ShouldRaiseException()
            {
                // Act & Assert
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    new Currency(Currency.MaxValue));
            }
        }
    }
}