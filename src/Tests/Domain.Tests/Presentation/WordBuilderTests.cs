﻿using Console.Host.Presentation;
using NUnit.Framework;

namespace Domain.Tests.Presentation
{
    [TestFixture]
    internal partial class WordBuilderTests
    {
        internal sealed class WhenToString
        {
            [TestCase(0, "zero dollars")]
            [TestCase(-1, "minus one dollar")]
            [TestCase(1, "one dollar")]
            [TestCase(25.1, "twenty-five dollars and ten cents")]
            [TestCase(0.01, "zero dollars and one cent")]
            [TestCase(45100, "forty-five thousand one hundred dollars")]
            [TestCase(999999999,
                "nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars")]
            [TestCase(123456789,
                "one hundred twenty-three million four hundred fifty-six thousand seven hundred eighty-nine dollars")]
            [TestCase(999999999.99,
                "nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars and ninety-nine cents")]
            [TestCase(123456789.12,
                "one hundred twenty-three million four hundred fifty-six thousand seven hundred eighty-nine dollars and twelve cents")]
            public void ShouldGetWithExpectedValues(decimal number, string wordPresentation)
            {
                // Arrange
                var visitor = new WordBuilder();

                // Act
                var result = visitor.Build(number);

                // Assert
                Assert.That(result, Is.EqualTo(wordPresentation));
            }
        }
    }
}