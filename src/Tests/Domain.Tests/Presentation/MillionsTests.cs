﻿using Console.Host.Presentation.Expressions;
using NUnit.Framework;

namespace Domain.Tests.Presentation
{
    internal partial class MillionsTests
    {
        internal sealed class WhenCreateMillions
        {
            [TestCase(0, null)]
            [TestCase(12, null)]
            [TestCase(123, null)]
            [TestCase(1234, null)]
            [TestCase(12345, null)]
            [TestCase(123456, null)]
            [TestCase(1234567, 1)]
            [TestCase(12345678, 12)]
            [TestCase(123456789, 123)]
            [TestCase(999999999.99, 999)]
            public void ShouldTakeExpectedValue(decimal number, int? expectedValue)
            {
                // Act & Arrange
                var millions = new Millions(number);

                // Assert
                Assert.That(millions.Number, Is.EqualTo(expectedValue));
            }
        }
    }
}