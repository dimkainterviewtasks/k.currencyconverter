﻿using Console.Host.Presentation.Expressions;
using NUnit.Framework;

namespace Domain.Tests.Presentation
{
    internal partial class HundredsTests
    {
        internal sealed class WhenCreateHundreds
        {
            [TestCase(0, null)]
            [TestCase(12, 12)]
            [TestCase(123, 123)]
            [TestCase(1234, 234)]
            [TestCase(12345, 345)]
            [TestCase(123456, 456)]
            [TestCase(1234567, 567)]
            [TestCase(12345678, 678)]
            [TestCase(123456789, 789)]
            [TestCase(999999999.99, 999)]
            public void ShouldTakeExpectedValue(decimal number, int? expectedValue)
            {
                // Act & Arrange
                var hundreds = new Hundreds(number);

                // Assert
                Assert.That(hundreds.Number, Is.EqualTo(expectedValue));
            }
        }
    }
}