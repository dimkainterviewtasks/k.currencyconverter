﻿using Console.Host.Presentation.Expressions;
using NUnit.Framework;

namespace Domain.Tests.Presentation
{
    internal partial class ThousandsTests
    {
        internal sealed class WhenCreateThousands
        {
            [TestCase(0, null)]
            [TestCase(12, null)]
            [TestCase(123, null)]
            [TestCase(1234, 1)]
            [TestCase(12345, 12)]
            [TestCase(123456, 123)]
            [TestCase(1234567, 234)]
            [TestCase(12345678, 345)]
            [TestCase(123456789, 456)]
            [TestCase(999999999.99, 999)]
            public void ShouldTakeExpectedValue(decimal number, int? expectedValue)
            {
                // Act & Arrange
                var thousands = new Thousands(number);

                // Assert
                Assert.That(thousands.Number, Is.EqualTo(expectedValue));
            }
        }
    }
}