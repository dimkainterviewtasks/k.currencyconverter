﻿using Console.Host.Presentation.Expressions;
using NUnit.Framework;

namespace Domain.Tests.Presentation
{
    [TestFixture]
    internal partial class CentTests
    {
        internal sealed class WhenCreateCents
        {
            [TestCase(56.9, 90)]
            [TestCase(0.01, 1)]
            [TestCase(0.12, 12)]
            [TestCase(0.123, 12)]
            [TestCase(999999999.99, 99)]
            public void ShouldTakeExpectedValue(decimal number, int expectedValue)
            {
                // Act & Arrange
                var cents = new Cent(number);

                // Assert
                Assert.That(cents.Number, Is.EqualTo(expectedValue));
            }
        }
    }
}