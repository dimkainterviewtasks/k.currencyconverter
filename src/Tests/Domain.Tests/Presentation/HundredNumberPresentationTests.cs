﻿using Console.Host.Presentation;
using NUnit.Framework;

namespace Domain.Tests.Presentation
{
    [TestFixture]
    internal partial class HundredNumberPresentationTests
    {
        internal sealed class WhenToString
        {
            [TestCase(112, "one hundred twelve")]
            [TestCase(999, "nine hundred ninety-nine")]
            [TestCase(909, "nine hundred nine")]
            [TestCase(990, "nine hundred ninety")]
            [TestCase(99, "ninety-nine")]
            [TestCase(9, "nine")]
            public void ShouldGetWithExpectedValues(int number, string wordPresentation)
            {
                // Arrange
                var numberPresentation = new HundredNumberPresentation(number);

                // Act
                var result = numberPresentation.ToString();

                // Assert
                Assert.That(result, Is.EqualTo(wordPresentation));
            }
        }
    }
}