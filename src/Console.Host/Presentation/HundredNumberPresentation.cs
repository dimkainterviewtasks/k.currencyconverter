﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Console.Host.Presentation
{
    internal sealed class HundredNumberPresentation
    {
        private readonly int _dozens;
        private readonly int _hundreds;
        private readonly int _units;

        private readonly Dictionary<int, string> _dozensExceptionsDictionary = new Dictionary<int, string>
        {
            {10, "ten"},
            {12, "twelve"},
            {13, "thirteen"},
            {14, "fourteen"},
            {15, "fifteen"},
            {16, "sixteen"},
            {17, "seventeen"},
            {18, "eighteen"},
            {19, "nineteen"}
        };

        private readonly Dictionary<int, string> _dozensValueDictionary = new Dictionary<int, string>
        {
            {2, "twenty"},
            {3, "thirty"},
            {4, "forty"},
            {5, "fifty"},
            {6, "sixty"},
            {7, "seventy"},
            {8, "eighty"},
            {9, "ninety"}
        };

        private readonly Dictionary<int, string> _unitsValueDictionary = new Dictionary<int, string>
        {
            {1, "one"},
            {2, "two"},
            {3, "three"},
            {4, "four"},
            {5, "five"},
            {6, "six"},
            {7, "seven"},
            {8, "eight"},
            {9, "nine"}
        };

        public HundredNumberPresentation(int number)
        {
            _hundreds = GetHundredsValue(number);
            _dozens = GetDozensValue(number);
            if (_dozens == 1)
                _dozens = number % 100;
            else
                _units = GetUnitsValue(number);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(_hundreds != 0 ? _unitsValueDictionary[_hundreds] : string.Empty);
            builder.Append(_hundreds != 0 ? " hundred" : string.Empty);
            builder.Append(_hundreds != 0 && (_dozens != 0 || _units != 0) ? " " : string.Empty);

            if (_dozens < 10)
            {
                builder.Append(_dozens != 0 ? _dozensValueDictionary[_dozens] : string.Empty);
                builder.Append(_dozens != 0 && _units != 0 ? "-" : string.Empty);

                builder.Append(_units != 0 ? _unitsValueDictionary[_units] : string.Empty);
            }
            else
            {
                builder.Append(_dozens != 0 ? _dozensExceptionsDictionary[_dozens] : string.Empty);
            }

            return builder.ToString();
        }

        private int GetHundredsValue(int number)
        {
            number = Math.Abs(number);
            return number / 100 % 10;
        }

        private int GetDozensValue(int number)
        {
            number = Math.Abs(number);
            return number / 10 % 10;
        }

        private int GetUnitsValue(int number)
        {
            number = Math.Abs(number);
            return number % 10;
        }
    }
}