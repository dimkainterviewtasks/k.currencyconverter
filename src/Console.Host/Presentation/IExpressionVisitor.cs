﻿using Console.Host.Presentation.Expressions;

namespace Console.Host.Presentation
{
    interface IExpressionVisitor
    {
        void Visit(Sign sign);

        void Visit(Millions millions);
        void Visit(Thousands thousands);
        void Visit(Hundreds hundreds);
        void Visit(Cent cents);
    }
}