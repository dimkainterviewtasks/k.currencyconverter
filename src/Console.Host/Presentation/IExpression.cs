﻿namespace Console.Host.Presentation
{
    interface IExpression
    {
        void Accept(IExpressionVisitor visitor);
    }
}