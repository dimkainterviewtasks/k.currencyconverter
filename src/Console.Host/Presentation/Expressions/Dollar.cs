﻿using System;

namespace Console.Host.Presentation.Expressions
{
    internal sealed class Dollar : IExpression
    {
        public Dollar(decimal number)
        {
            Millions = new Millions(number);
            Thousands = new Thousands(number);
            Hundreds = new Hundreds(number);
        }

        internal IExpression Millions { get; }
        internal IExpression Thousands { get; }
        internal IExpression Hundreds { get; }

        public void Accept(IExpressionVisitor visitor)
        {
            if (visitor == null)
                throw new ArgumentNullException(nameof(visitor));

            Millions.Accept(visitor);
            Thousands.Accept(visitor);
            Hundreds.Accept(visitor);
        }
    }
}