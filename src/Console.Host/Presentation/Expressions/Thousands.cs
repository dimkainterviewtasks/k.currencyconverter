﻿using System;

namespace Console.Host.Presentation.Expressions
{
    internal sealed class Thousands : IExpression
    {
        public Thousands(decimal number)
        {
            var thousandPart = GetThousandPart(number);
            Number = thousandPart == 0 ? null : (int?) thousandPart;
        }

        public int? Number { get; }

        public void Accept(IExpressionVisitor visitor)
        {
            if (visitor == null)
                throw new ArgumentNullException(nameof(visitor));

            visitor.Visit(this);
        }

        private int GetThousandPart(decimal number)
        {
            number = Math.Abs(number);
            return (int)(number % 1000000 - number % 1000) / 1000;
        }
    }
}