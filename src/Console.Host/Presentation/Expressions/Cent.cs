﻿using System;

namespace Console.Host.Presentation.Expressions
{
    internal sealed class Cent : IExpression
    {
        public Cent(decimal number)
        {
            var fractionalPart = GetFractionalPart(number);
            Number = fractionalPart == 0 ? null : (int?) fractionalPart;
        }

        public int? Number { get; }

        public void Accept(IExpressionVisitor visitor)
        {
            if (visitor == null)
                throw new ArgumentNullException(nameof(visitor));

            visitor.Visit(this);
        }

        private int GetFractionalPart(decimal number)
        {
            number = Math.Abs(number);
            return (int) Math.Round((number - Math.Truncate(number)) * 100);
        }
    }
}