﻿using System;

namespace Console.Host.Presentation.Expressions
{
    internal sealed class Millions : IExpression
    {
        public Millions(decimal number)
        {
            var millionPart = GetMillionPart(number);
            Number = millionPart == 0 ? null : (int?) millionPart;
        }

        public int? Number { get; }

        public void Accept(IExpressionVisitor visitor)
        {
            if (visitor == null)
                throw new ArgumentNullException(nameof(visitor));

            visitor.Visit(this);
        }

        private int GetMillionPart(decimal number)
        {
            number = Math.Abs(number);
            return (int)(number % 1000000000 - number % 1000000) / 1000000;
        }
    }
}