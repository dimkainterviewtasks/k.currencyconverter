﻿using System;

namespace Console.Host.Presentation.Expressions
{
    internal sealed class CurrencyExpression : IExpression
    {
        public CurrencyExpression(decimal number)
        {
            Sign = new Sign(number);
            Dollars = new Dollar(number);
            Cents = new Cent(number);
        }

        public IExpression Sign { get; }
        public IExpression Dollars { get; }
        public IExpression Cents { get; }

        public void Accept(IExpressionVisitor visitor)
        {
            if (visitor == null)
                throw new ArgumentNullException(nameof(visitor));

            Sign.Accept(visitor);
            Dollars.Accept(visitor);
            Cents.Accept(visitor);
        }
    }
}