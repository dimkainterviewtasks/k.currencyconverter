﻿using System;

namespace Console.Host.Presentation.Expressions
{
    internal sealed class Sign : IExpression
    {
        public Sign(decimal? number)
        {
            Number = number == 0 ? null : number;
        }

        public decimal? Number { get; }

        public void Accept(IExpressionVisitor visitor)
        {
            if (visitor == null)
                throw new ArgumentNullException(nameof(visitor));

            visitor.Visit(this);
        }
    }
}