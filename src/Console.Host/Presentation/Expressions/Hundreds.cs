﻿using System;

namespace Console.Host.Presentation.Expressions
{
    internal sealed class Hundreds : IExpression
    {
        public Hundreds(decimal number)
        {
            var hundredPart = GetHundredPart(number);
            Number = hundredPart == 0 ? null : (int?) hundredPart;
        }

        public int? Number { get; }

        public void Accept(IExpressionVisitor visitor)
        {
            if (visitor == null)
                throw new ArgumentNullException(nameof(visitor));

            visitor.Visit(this);
        }

        private int GetHundredPart(decimal number)
        {
            number = Math.Abs(number);
            return (int) (number % 1000);
        }
    }
}