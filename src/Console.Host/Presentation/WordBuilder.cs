﻿using System.Text;
using Console.Host.Presentation.Expressions;

namespace Console.Host.Presentation
{
    internal sealed class WordBuilder : IExpressionVisitor
    {
        private readonly StringBuilder _builder;

        public WordBuilder()
        {
            _builder = new StringBuilder();
        }

        public void Visit(Sign sign)
        {
            var value = sign.Number < 0
                ? "minus"
                : null;
            _builder.Append(value);
            AppendSpace(value);
        }

        public void Visit(Hundreds hundreds)
        {
            var value = hundreds.Number.HasValue
                ? new HundredNumberPresentation(hundreds.Number.Value).ToString()
                : null;
            _builder.Append(value);

            var isPlural = hundreds.Number.HasValue && hundreds.Number.Value != 0 && hundreds.Number.Value % 10 != 1;
            AppendWord(value, isPlural ? "dollars" : "dollar");

            if (_builder.Length == 0)
                _builder.Append("zero dollars");
        }

        public void Visit(Thousands thousands)
        {
            var value = thousands.Number.HasValue
                ? $"{new HundredNumberPresentation(thousands.Number.Value)} thousand"
                : null;
            _builder.Append(value);
            AppendSpace(value);
        }

        public void Visit(Millions millions)
        {
            var value = millions.Number.HasValue
                ? $"{new HundredNumberPresentation(millions.Number.Value)} million"
                : null;

            _builder.Append(value);
            AppendSpace(value);
        }

        public void Visit(Cent cents)
        {
            var value = cents.Number.HasValue
                ? $"{new HundredNumberPresentation(cents.Number.Value)}"
                : null;
            var isPlural = cents.Number.HasValue && cents.Number.Value != 0 && cents.Number.Value % 10 != 1;
            AppendWord(value, "and", value, isPlural ? "cents" : "cent");
        }

        public string Build(decimal number)
        {
            var currencyExpression = new CurrencyExpression(number);
            currencyExpression.Accept(this);

            return _builder.ToString();
        }

        private void AppendSpace(string word)
        {
            if (word != null)
                _builder.Append(" ");
        }

        private void AppendWord(string previousWord, params string[] words)
        {
            if (previousWord != null)
            {
                foreach (var word in words)
                {
                    _builder.Append(" ");
                    _builder.Append(word);
                }
            }
        }
    }
}