﻿using System;
using System.Text;
using Console.Host.Presentation;
using Domain.Application;

namespace Console.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            var currencyService = new CurrencyService();

            var standardForegroundColor = System.Console.ForegroundColor;
            
            WriteLine("================================", ConsoleColor.Red);
            WriteLine("== To exit please write: exit ==", ConsoleColor.Red);
            WriteLine("================================", ConsoleColor.Red);

            while (true)
            {
                WriteLine("Please, write a number to convert to word presentation!", ConsoleColor.Green);

                System.Console.ForegroundColor = ConsoleColor.White;
                var number = System.Console.ReadLine();
                if (number == "exit")
                    break;

                try
                {
                    var currencyDto = currencyService.GetCurrency(number);

                    var wordBuilder = new WordBuilder();
                    var word = wordBuilder.Build(currencyDto.Value);
                    
                    Write($"The number {number} has word presentation: ", standardForegroundColor);
                    WriteLine($"{word}", ConsoleColor.Green);
                }
                catch (ArgumentOutOfRangeException)
                {
                    WriteLine("Input data is out of range", ConsoleColor.Red);
                }
                catch (ArgumentException)
                {
                    WriteLine("Input data is wrong", ConsoleColor.Red);
                }

                WriteLine("________________________________", ConsoleColor.White);
                System.Console.WriteLine("");
            }
        }

        private static void Write(string str, ConsoleColor color)
        {
            System.Console.ForegroundColor = color;
            System.Console.Write(str);
        }

        private static void WriteLine(string str, ConsoleColor color)
        {
            System.Console.ForegroundColor = color;
            System.Console.WriteLine(str);
        }
    }
}
