﻿using System;
using System.Globalization;
using Currency.Contracts;

namespace Domain.Application
{
    public sealed class CurrencyService
    {
        public CurrencyDto GetCurrency(string number)
        {
            var provider = new NumberFormatInfo {NumberDecimalSeparator = ",", NumberGroupSeparator = " "};
            if (!decimal.TryParse(number, NumberStyles.Number, provider, out var result))
                throw new ArgumentException("Input string is not a number");

            var currency = new Currency(result);

            return new CurrencyDto(currency.Value);
        }
    }
}