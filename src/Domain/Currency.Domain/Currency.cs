﻿using System;

namespace Domain
{
    internal sealed class Currency
    {
        internal const int MaxValue = 1000000000;

        public Currency(decimal value)
        {
            if (value >= MaxValue)
                throw new ArgumentOutOfRangeException();

            Value = value;
        }

        public decimal Value { get; }
    }
}