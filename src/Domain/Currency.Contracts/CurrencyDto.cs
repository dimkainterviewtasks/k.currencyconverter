﻿using System;

namespace Currency.Contracts
{
    public sealed class CurrencyDto
    {
        public CurrencyDto(decimal value)
        {
            Value = value;
        }

        public decimal Value { get; }
    }
}
