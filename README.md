# K.CurrencyConverter

Here I will try to answer some possible questions which could be raised when you see this implementation:

1.	Why do we have a Domain folder for such a simple application?
		Well, I can see at least one business rule (MAX number is 999999999). And BR should be checked in a domain.

2.	What is the CurrencyService for?
	Why Currency is internal?
	What is the CurrencyDto for?
	 Currency is internal because it's a domain object and we need to keep it protected.
	 CurrencyService is a thing that knows how to communicate with domain.
        It contains API for communication.
		It contains an anticorruption layer that doesn't allow wrong data to flow to domain.
		If data is correct it knows how to input data to convert to domain data.
	 CurrencyDto needs to transfer information to a client. You need something if you don't want to share a domain structure.

3. Why do parsing numbers to words presentation code is in Host application?
	Well, Console app iat the same time is a host app and presentation app.
	Users can communicate with the application through this project.
	And I believe the parsing belongs to the presentation layer because if you want other output of data you don't need to change domain now.
	By the way, now you just need to implement a new implementation of IExpressionVisitor and reuse it in WordPresentation.
	Actually WordPresentation probably is not so good because in the case you need to rewrite it completely or just remove it.
	But I guess for test task is ok to go with such implementation.

4. Why do I use a visitor pattern?
	Well, the code was working with a different implementation. You can take a look on it (commit "Moved presentation logic from domain." 02.10.2019)
	But this implementation was really unreadable. But it contained much fewer files. Well, there is no silver bullet. 

4. Why do the projects use netcore & netstandart?
	I use GitLab CI pipeline to check code when I commit something. For this reason I want to use net SDK to compile code & run tests.